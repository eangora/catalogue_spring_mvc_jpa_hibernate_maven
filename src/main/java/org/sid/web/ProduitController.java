package org.sid.web;

import java.util.List;

import org.sid.dao.ProduitRepository;
import org.sid.entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProduitController {
	@Autowired
	private ProduitRepository produitRepository;
	
	@RequestMapping("/index")
	public String index(Model model){
		List<Produit> produits= produitRepository.findAll();
		model.addAttribute("listProduits",produits);
		return "produits";
	}
	//On affiche les résultats en fonction du nombre de page definie
	@RequestMapping("/index2")
	public String produit(Model model,
			@RequestParam(name="page",defaultValue="0")int p,
			@RequestParam(name="page",defaultValue="5")int s){
		Page<Produit> pageproduits= produitRepository.findAll(PageRequest.of(p, s));
		model.addAttribute("listProduits",pageproduits.getContent());
		//ondétermine le nombre de page
		int[] pages = new int[pageproduits.getTotalPages()];
		//On le stock dans le model
		model.addAttribute("pages",pages);
		return "produits";
	}
	
	
	
}
